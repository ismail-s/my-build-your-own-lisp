import subprocess, sys

p = subprocess.Popen(['./lisp', 'stdlib.lspy', 'test.lspy', '--no-repl'], stdout = subprocess.PIPE)
out, _ = p.communicate()
out = out.decode()
print(out)
if "Test failed" in out or "Error: " in out:
    sys.exit(1)
