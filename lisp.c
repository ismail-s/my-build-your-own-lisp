#include <stdio.h>
#include <stdlib.h>

#include "mpc.h"

#ifdef _WIN32
#include <string.h>

static char buffer[2048];

char* readline(char* prompt) {
  fputs(prompt, stdout);
  fgets(buffer, 2048, stdin);
  char* cpy = malloc(strlen(buffer) + 1);
  strcpy(cpy, buffer);
  cpy[strlen(cpy) - 1] = '\0';
  return cpy;
}

void add_history(char* unused) {}

#else
#include <editline/readline.h>
#include <editline/history.h>
#endif

mpc_parser_t* Number;
mpc_parser_t* Symbol;
mpc_parser_t* String;
mpc_parser_t* Comment;
mpc_parser_t* Sexpr;
mpc_parser_t* Qexpr;
mpc_parser_t* Expr;
mpc_parser_t* Lispy;

struct lval;
struct lenv;

typedef struct lval lval;
typedef struct lenv lenv;

enum { LVAL_NUM, LVAL_ERR, LVAL_SYM, LVAL_STR, LVAL_FUN, LVAL_SEXPR, LVAL_QEXPR };

typedef lval*(*lbuiltin)(lenv*, lval*);

struct lval {
  int type;

  // Basic
  long num;
  // Error and symbol types have some string data
  char* err;
  char* sym;
  char* str;

  // Function
  lbuiltin builtin;
  lenv* env;
  lval* formals;
  lval* body;

  // Expression
  // Count and pointer to a list of lval*
  int count;
  lval** cell;
};

struct lenv {
  lenv* par; // parent lenv
  int count;
  char** syms;
  lval** vals;
};

char* ltype_name(int i) {
  switch (i) {
  case LVAL_FUN: return "Function";
  case LVAL_NUM: return "Number";
  case LVAL_ERR: return "Error";
  case LVAL_SYM: return "Symbol";
  case LVAL_STR: return "String";
  case LVAL_SEXPR: return "S-expression";
  case LVAL_QEXPR: return "Q-expression";
  default: return "Unknown";
  }
}

lenv* lenv_new(void) {
  lenv* e = malloc(sizeof(lenv));
  e->count = 0;
  e->par = NULL;
  e->syms = NULL;
  e->vals = NULL;
  return e;
}

void lval_del(lval* v);

void lenv_del(lenv* e) {
  for (int i = 0; i < e->count; i++) {
    free(e->syms[i]);
    lval_del(e->vals[i]);
  }
  free(e->syms);
  free(e->vals);
  free(e);
}

lval* lval_err(char* m, ...);
lval* lval_copy(lval* v);

lval* lenv_get(lenv* e, lval* k) {
  for (int i = 0; i < e->count; i++) {
    if (strcmp(e->syms[i], k->sym) == 0) {
      return lval_copy(e->vals[i]);
    }
  }
  if (e->par) {
    return lenv_get(e->par, k);
  } else {
    return lval_err("unbound symbol '%s'", k->sym);
  }
}

void lenv_put(lenv* e, lval* k, lval* v) {
  for (int i = 0; i < e->count; i++) {
    if (strcmp(e->syms[i], k->sym) == 0) {
      lval_del(e->vals[i]);
      e->vals[i] = lval_copy(v);
      return;
    }
  }
  e->count++;
  e->vals = realloc(e->vals, sizeof(lval*) * e->count);
  e->syms = realloc(e->syms, sizeof(char*) * e->count);
  e->vals[e->count-1] = lval_copy(v);
  e->syms[e->count-1] = malloc(strlen(k->sym) + 1);
  strcpy(e->syms[e->count-1], k->sym);
}

void lenv_def(lenv* e, lval* k, lval* v) {
  while (e->par) { e = e->par; }
  lenv_put(e, k, v);
}

lenv* lenv_copy(lenv* e) {
  lenv* n = malloc(sizeof(lenv));
  n->par = e->par;
  n->count = e->count;
  n->syms = malloc(sizeof(char*) * e->count);
  n->vals = malloc(sizeof(lval*) * e->count);
  for (int i = 0; i < e->count; i++) {
    n->syms[i] = malloc(strlen(e->syms[i]) + 1);
    strcpy(n->syms[i], e->syms[i]);
    n->vals[i] = lval_copy(e->vals[i]);
  }
  return n;
}

lval* lval_num(long num) {
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_NUM;
  v->num = num;
  return v;
}

lval* lval_err(char* fmt, ...) {
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_ERR;
  va_list va;
  va_start(va, fmt);
  v->err = malloc(512);
  vsnprintf(v->err, 511, fmt, va);
  v->err = realloc(v->err, strlen(v->err) + 1);
  va_end(va);
  return v;
}

lval* lval_sym(char* s) {
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_SYM;
  v->sym = malloc(strlen(s) + 1);
  strcpy(v->sym, s);
  return v;
}

lval* lval_str(char* s) {
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_STR;
  v->str = malloc(strlen(s) + 1);
  strcpy(v->str, s);
  return v;
}

lval* lval_sexpr(void) {
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_SEXPR;
  v->count = 0;
  v->cell = NULL;
  return v;
}

lval* lval_qexpr(void) {
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_QEXPR;
  v->count = 0;
  v->cell = NULL;
  return v;
}

lval* lval_fun(lbuiltin func) {
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_FUN;
  v->builtin = func;
  return v;
}

lval* lval_lambda(lval* formals, lval* body) {
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_FUN;
  v->builtin = NULL;
  v->env = lenv_new();
  v->formals = formals;
  v->body = body;
  return v;
}

void lval_del(lval* v) {
  switch (v->type) {
  case LVAL_NUM: break;
  case LVAL_FUN:
    if (!v->builtin) {
      lenv_del(v->env);
      lval_del(v->formals);
      lval_del(v->body);
    }
    break;
  case LVAL_ERR: free(v->err); break;
  case LVAL_SYM: free(v->sym); break;
  case LVAL_STR: free(v->str); break;
  case LVAL_QEXPR:
  case LVAL_SEXPR:
    for (int i = 0; i < v->count; i++) {
      lval_del(v->cell[i]);
    }
    free(v->cell);
    break;
  }
  free(v);
}

int lval_eq(lval* a, lval* b) {
  if (a->type != b->type) { return 0; }
  switch (a->type) {
  case LVAL_NUM: return a->num == b->num;
  case LVAL_ERR: return strcmp(a->err, b->err) == 0;
  case LVAL_SYM: return strcmp(a->sym, b->sym) == 0;
  case LVAL_STR: return strcmp(a->str, b->str) == 0;
  case LVAL_FUN:
    if (a->builtin || b->builtin) { return a->builtin == b->builtin; }
    return a->env == b->env && lval_eq(a->formals, b->formals) && lval_eq(a->body, b->body);
  case LVAL_SEXPR:
  case LVAL_QEXPR:
    if (a->count != b->count) { return 0; }
    for (int i = 0; i < a->count; i++) {
      if (!lval_eq(a->cell[i], b->cell[i])) { return 0; }
    }
    return 1;
  break;
  }
  return 0;
}

lval* lval_copy(lval* v) {
  lval* x = malloc(sizeof(lval));
  x->type = v->type;
  switch (v->type) {
  case LVAL_NUM: x->num = v->num; break;
  case LVAL_FUN:
    if (v->builtin) {
      x->builtin = v->builtin;
    } else {
      x->builtin = NULL;
      x->env = lenv_copy(v->env);
      x->formals = lval_copy(v->formals);
      x->body = lval_copy(v->body);
    }
    break;
  case LVAL_ERR:
    x->err = malloc(strlen(v->err) + 1);
    strcpy(x->err, v->err); break;
  case LVAL_SYM:
    x->sym = malloc(strlen(v->sym) + 1);
    strcpy(x->sym, v->sym); break;
  case LVAL_STR:
    x->str = malloc(strlen(v->str) + 1);
    strcpy(x->str, v->str); break;
  case LVAL_SEXPR:
  case LVAL_QEXPR:
    x->count = v->count;
    x->cell = malloc(sizeof(lval*) * v->count);
    for (int i = 0; i < v->count; i++) {
      x->cell[i] = lval_copy(v->cell[i]);
    }
    break;
  }
  return x;
}

lval* lval_read_num(mpc_ast_t* t) {
  errno = 0;
  long num = strtol(t->contents, NULL, 10);
  return errno != ERANGE ?
    lval_num(num) : lval_err("invalid number");
}

lval* lval_read_str(mpc_ast_t* t) {
  t->contents[strlen(t->contents)-1] = '\0';
  char* unescaped = malloc(strlen(t->contents+1)+1);
  strcpy(unescaped, t->contents+1);
  unescaped = mpcf_unescape(unescaped);
  lval* str = lval_str(unescaped);
  free(unescaped);
  return str;
}

lval* lval_add(lval* v, lval* x) {
  v->count++;
  v->cell = realloc(v->cell, sizeof(lval*) * v->count);
  v->cell[v->count - 1] = x;
  return v;
}

lval* lval_read(mpc_ast_t* t) {
  if (strstr(t->tag, "number")) { return lval_read_num(t); }
  if (strstr(t->tag, "symbol")) { return lval_sym(t->contents); }
  if (strstr(t->tag, "string")) { return lval_read_str(t); }
  lval* x = NULL;
  if (strcmp(t->tag, ">") == 0 || strstr(t->tag, "sexpr")) { x = lval_sexpr(); }
  if (strstr(t->tag, "qexpr")) { x = lval_qexpr(); }
  for (int i = 0; i < t->children_num; i++) {
    if (strcmp(t->children[i]->contents, "(") == 0) { continue; }
    if (strcmp(t->children[i]->contents, ")") == 0) { continue; }
    if (strcmp(t->children[i]->contents, "{") == 0) { continue; }
    if (strcmp(t->children[i]->contents, "}") == 0) { continue; }
    if (strcmp(t->children[i]->tag, "regex") == 0) { continue; }
    if (strstr(t->children[i]->tag, "comment")) { continue; }
    x = lval_add(x, lval_read(t->children[i]));
  }
  return x;
}

void lval_print(lval* v);

void lval_expr_print(lval* v, char open, char close) {
  putchar(open);
  for (int i = 0; i < v->count; i++) {
    lval_print(v->cell[i]);
    if (i != (v->count - 1)) {
      putchar(' ');
    }
  }
  putchar(close);
}

void lval_print_str(lval* v) {
  char* escaped = malloc(strlen(v->str) + 1);
  strcpy(escaped, v->str);
  escaped = mpcf_escape(escaped);
  printf("\"%s\"", escaped);
  free(escaped);
}

void lval_print(lval* v) {
  switch (v->type) {
  case LVAL_NUM: printf("%li", v->num); break;
  case LVAL_ERR: printf("Error: %s", v->err); break;
  case LVAL_SYM: printf("%s", v->sym); break;
  case LVAL_STR: lval_print_str(v); break;
  case LVAL_SEXPR: lval_expr_print(v, '(', ')'); break;
  case LVAL_QEXPR: lval_expr_print(v, '{', '}'); break;
  case LVAL_FUN:
    if (v->builtin) {
      printf("<function>");
    } else {
      printf("(\\ "); lval_print(v->formals);
      putchar(' '); lval_print(v->body); putchar(')');
    }
    break;
  }
}

void lval_println(lval* v) { lval_print(v); putchar('\n'); }

lval* lval_pop(lval* v, int i) {
  lval* x = v->cell[i];
  memmove(&v->cell[i], &v->cell[i+1], sizeof(lval*) * (v->count - i - 1));
  v->count--;
  v->cell = realloc(v->cell, sizeof(lval*) * v->count);
  return x;
}

lval* lval_take(lval* v, int i) {
  lval* x = lval_pop(v, i);
  lval_del(v);
  return x;
}

#define LASSERT(args, cond, fmt, ...) \
  if (!(cond)) { \
    lval* err = lval_err(fmt, ##__VA_ARGS__); \
    lval_del(args); \
    return err; \
  }

#define LASSERT_NUM(func, args, num) \
  LASSERT(args, args->count == num, \
	  "Function '%s' passed incorrent number of arguments. Got %i. Expected %i.", \
	  func, args->count, num);

#define LASSERT_TYPE(func, args, index, expect)	\
  LASSERT(args, args->cell[index]->type == expect, \
	  "Function '%s' passed incorrent type for argument %i. Got %s. Expected %s.", \
	  func, index, ltype_name(args->cell[index]->type), ltype_name(expect));

lval* builtin_head(lenv* e, lval* a) {
  LASSERT(a, a->count == 1, "Function 'head' passed too many arguments. Got %i. Expected %i.", a->count, 1);
  LASSERT(a, a->cell[0]->type == LVAL_QEXPR, "Function 'head' not passed a Q-expression. Was passed a %s instead.", ltype_name(a->cell[0]->type));
  LASSERT(a, a->cell[0]->count != 0, "Function 'head' pased {}");
  lval* v = lval_take(a, 0);
  while (v->count > 1) { lval_del(lval_pop(v, 1)); }
  return v;
}

lval* builtin_tail(lenv* e, lval* a) {
  LASSERT(a, a->count == 1, "Function 'tail' passed too many arguments");
  LASSERT(a, a->cell[0]->type == LVAL_QEXPR, "Function 'tail' not passed a Q-expression");
  LASSERT(a, a->cell[0]->count != 0, "Function 'tail' pased {}");
  lval* v = lval_take(a, 0);
  lval_del(lval_pop(v, 0));
  return v;
}

lval* builtin_list(lenv* e, lval* a) {
  a->type = LVAL_QEXPR;
  return a;
}

lval* lval_eval(lenv* e, lval* v);

lval* builtin_eval(lenv* e, lval* a) {
  LASSERT(a, a->count == 1, "Function 'eval' passed too many arguments");
  LASSERT(a, a->cell[0]->type == LVAL_QEXPR, "Function 'eval' not passed a Q-expression");
  lval* x = lval_take(a, 0);
  x->type = LVAL_SEXPR;
  return lval_eval(e, x);
}

lval* lval_join(lval* x, lval* y) {
  while (y->count) {
    x = lval_add(x, lval_pop(y, 0));
  }
  lval_del(y);
  return x;
}

lval* builtin_join(lenv* e, lval* a) {
  for (int i = 0; i < a->count; i++) {
    LASSERT(a, a->cell[i]->type == LVAL_QEXPR, "Function 'join' passed something that isn't a Q-expression")
  }
  lval* x = lval_pop(a, 0);
  while (a->count) {
    x = lval_join(x, lval_pop(a, 0));
  }
  lval_del(a);
  return x;
}

lval* builtin_op(lval* a, char* op) {
  for (int i = 0; i < a->count; i++) {
    if (a->cell[i]->type != LVAL_NUM) {
      lval_del(a);
      return lval_err("Can't operate on non-numbers");
    }
  }
  lval* x = lval_pop(a, 0);
  if (strcmp(op, "-") == 0 && a->count == 0) {
    x->num = -x->num;
  }
  while (a->count > 0) {
    lval* y = lval_pop(a, 0);
    if (strcmp(op, "+") == 0) { x->num += y->num; }
    if (strcmp(op, "-") == 0) { x->num -= y->num; }
    if (strcmp(op, "*") == 0) { x->num *= y->num; }
    if (strcmp(op, "/") == 0) {
      if (y->num == 0) {
	lval_del(x);
	lval_del(y);
	x = lval_err("Division by zero");
	break;
      }
      x->num /= y->num;
    }
    lval_del(y);
  }
  lval_del(a);
  return x;
}

lval* builtin_add(lenv* e, lval* a) {
  return builtin_op(a, "+");
}

lval* builtin_sub(lenv* e, lval* a) {
  return builtin_op(a, "-");
}

lval* builtin_mul(lenv* e, lval* a) {
  return builtin_op(a, "*");
}

lval* builtin_div(lenv* e, lval* a) {
  return builtin_op(a, "/");
}

lval* builtin_compare(lval* a, char* cmp) {
  LASSERT_NUM(cmp, a, 2);
  LASSERT_TYPE(cmp, a, 0, LVAL_NUM);
  LASSERT_TYPE(cmp, a, 1, LVAL_NUM);
  int x = a->cell[0]->num;
  int y = a->cell[1]->num;
  int comparison;
  if (strcmp(cmp, "<") == 0) { comparison = x < y; }
  else if (strcmp(cmp, ">") == 0) { comparison = x > y; }
  else if (strcmp(cmp, ">=") == 0) { comparison = x >= y; }
  else if (strcmp(cmp, "<=") == 0) { comparison = x <= y; }
  else { return lval_err("Internal error in builtin_compare function"); }
  lval_del(a);
  if (comparison) {
    return lval_num(1);
  } else {
    return lval_num(0);
  }
}

lval* builtin_greater_than(lenv* e, lval* a) {
  return builtin_compare(a, ">");
}

lval* builtin_less_than(lenv* e, lval* a) {
  return builtin_compare(a, "<");
}

lval* builtin_greater_than_or_equals(lenv* e, lval* a) {
  return builtin_compare(a, ">=");
}

lval* builtin_less_than_or_equals(lenv* e, lval* a) {
  return builtin_compare(a, "<=");
}

lval* builtin_equals_or_not(lval* a, char* cmp) {
  LASSERT_NUM(cmp, a, 2);
  int result = lval_eq(a->cell[0], a->cell[1]);
  lval_del(a);
  if (strcmp(cmp, "==") == 0) { return lval_num(result); }
  else if (strcmp(cmp, "!=") == 0) { return lval_num(!result); }
  else { return lval_err("Internal error in builtin_equals_or_not function"); }
}

lval* builtin_equal(lenv* e, lval* a) {
  return builtin_equals_or_not(a, "==");
}

lval* builtin_not_equal(lenv* e, lval* a) {
  return builtin_equals_or_not(a, "!=");
}

lval* builtin_var(lenv* e, lval* a, char* func) {
  LASSERT_TYPE(func, a, 0, LVAL_QEXPR);
  lval* syms = a->cell[0];
  for (int i = 0; i < syms->count; i++) {
    LASSERT(a, syms->cell[i]->type == LVAL_SYM, "Function 'def' can't define a non-symbol");
  }
  LASSERT(a, syms->count == a->count-1, "Function 'def' can't define incorrect number of symbols to values");
  for (int i = 0; i < syms->count; i++) {
    if (strcmp(func, "def") == 0) {
      lenv_def(e, syms->cell[i], a->cell[i+1]);
    }
    if (strcmp(func, "=") == 0) {
      lenv_put(e, syms->cell[i], a->cell[i+1]);
    }
  }
  lval_del(a);
  return lval_sexpr();
}

lval* builtin_def(lenv* e, lval* a) {
  return builtin_var(e, a, "def");
}

lval* builtin_put(lenv* e, lval* a) {
  return builtin_var(e, a, "=");
}

lval* builtin_lambda(lenv* e, lval* a) {
  LASSERT_NUM("\\", a, 2);
  LASSERT_TYPE("\\", a, 0, LVAL_QEXPR);
  LASSERT_TYPE("\\", a, 1, LVAL_QEXPR);
  for (int i = 0; i < a->cell[0]->count; i++) {
    LASSERT(a, a->cell[0]->cell[i]->type == LVAL_SYM,
	    "Can't define non-symbol. Got %s. Expected %s.",
	    ltype_name(a->cell[0]->cell[i]->type), ltype_name(LVAL_SYM));
  }
  lval* formals = lval_pop(a, 0);
  lval* body = lval_pop(a, 0);
  lval_del(a);
  return lval_lambda(formals, body);
}

lval* builtin_if(lenv* e, lval* a) {
  LASSERT_NUM("if", a, 3);
  LASSERT_TYPE("if", a, 0, LVAL_NUM);
  LASSERT_TYPE("if", a, 1, LVAL_QEXPR);
  LASSERT_TYPE("if", a, 2, LVAL_QEXPR);
  lval* res;
  if (a->cell[0]->num) {
    res = lval_pop(a, 1);
  } else {
    res = lval_pop(a, 2);
  }
  lval_del(a);
  res->type = LVAL_SEXPR;
  return lval_eval(e, res);
}

lval* builtin_load(lenv* e, lval* a) {
  LASSERT_NUM("load", a, 1);
  LASSERT_TYPE("load", a, 0, LVAL_STR);
  mpc_result_t r;
  if (!mpc_parse_contents(a->cell[0]->str, Lispy, &r)) {
    char* err_msg = mpc_err_string(r.error);
    mpc_err_delete(r.error);
    lval* err = lval_err("Could not load Library %s", err_msg);
    free(err_msg);
    lval_del(a);
    return err;
  } else {
    lval* expr = lval_read(r.output);
    mpc_ast_delete(r.output);
    while (expr->count) {
      lval* x = lval_eval(e, lval_pop(expr, 0));
      if (x->type == LVAL_ERR) { lval_println(x); }
      lval_del(x);
    }
    lval_del(expr);
    lval_del(a);
    return lval_sexpr();
  }
}

lval* builtin_print(lenv* e, lval* a) {
  for (int i = 0; i < a->count; i++) {
    lval_print(a->cell[i]);
    putchar(' ');
  }
  putchar('\n');
  lval_del(a);
  return lval_sexpr();
}

lval* builtin_error(lenv* e, lval* a) {
  LASSERT_NUM("error", a, 1);
  LASSERT_TYPE("error", a, 0, LVAL_STR);
  lval* err = lval_err(a->cell[0]->str);
  lval_del(a);
  return err;
}

void lenv_add_builtin(lenv* e, char* name, lbuiltin func) {
  lval* k = lval_sym(name);
  lval* v = lval_fun(func);
  lenv_put(e, k, v);
  lval_del(k);
  lval_del(v);
}

void lenv_add_builtins(lenv* e) {
  lenv_add_builtin(e, "list", builtin_list);
  lenv_add_builtin(e, "head", builtin_head);
  lenv_add_builtin(e, "tail", builtin_tail);
  lenv_add_builtin(e, "eval", builtin_eval);
  lenv_add_builtin(e, "join", builtin_join);
  lenv_add_builtin(e, "+", builtin_add);
  lenv_add_builtin(e, "-", builtin_sub);
  lenv_add_builtin(e, "*", builtin_mul);
  lenv_add_builtin(e, "/", builtin_div);
  lenv_add_builtin(e, ">", builtin_greater_than);
  lenv_add_builtin(e, "<", builtin_less_than);
  lenv_add_builtin(e, ">=", builtin_greater_than_or_equals);
  lenv_add_builtin(e, "<=", builtin_less_than_or_equals);
  lenv_add_builtin(e, "==", builtin_equal);
  lenv_add_builtin(e, "!=", builtin_not_equal);
  lenv_add_builtin(e, "def", builtin_def);
  lenv_add_builtin(e, "=", builtin_put);
  lenv_add_builtin(e, "\\", builtin_lambda);
  lenv_add_builtin(e, "if", builtin_if);
  lenv_add_builtin(e, "load", builtin_load);
  lenv_add_builtin(e, "print", builtin_print);
  lenv_add_builtin(e, "error", builtin_error);
}

lval* add_arguments_to_function(lval* f, lval* v) {
  int given = v->count;
  int total = f->formals->count;
  while (v->count) {
    if (f->formals->count == 0) {
      lval_del(v); lval_del(f);
      return lval_err("Function passed too many arguments. Got %i. Expected %i.", given, total);
    }
    lval* sym = lval_pop(f->formals, 0);
    if (strcmp(sym->sym, "&") == 0) {
      if (f->formals->count != 1) {
	lval_del(v); lval_del(f);
	return lval_err("Function format invalid. Symbol '&' not followed by a single symbol.");
      }
      lval* nsym = lval_pop(f->formals, 0);
      lenv_put(f->env, nsym, builtin_list(f->env, v));
      lval_del(sym); lval_del(nsym);
      break;
    }
    lval* val = lval_pop(v, 0);
    lenv_put(f->env, sym, val);
    lval_del(sym); lval_del(val);
  }
  lval_del(v);
  return f;
}

lval* add_empty_list_to_function_if_necessary(lval* f) {
  if (f->formals->count > 0 && strcmp(f->formals->cell[0]->sym, "&") == 0) {
    if (f->formals->count != 2) {
      lval_del(f);
      return lval_err("Function format invalid. Symbol '&' not followed by a single symbol.");
    }
    lval_del(lval_pop(f->formals, 0));
    lval* sym = lval_pop(f->formals, 0);
    lval* val = lval_qexpr();
    lenv_put(f->env, sym, val);
    lval_del(sym); lval_del(val);
  }
  return f;
}

lval* lval_eval(lenv* e, lval* v) {
  while (1) {
    switch (v->type) {
    case LVAL_SYM: ;
      lval* x = lenv_get(e, v);
      lval_del(v);
      return x;
    case LVAL_SEXPR:
      // Eval each element of the s-expression
      for (int i = 0; i < v->count; i++) {
	v->cell[i] = lval_eval(e, v->cell[i]);
	if (v->cell[i]->type == LVAL_ERR) { return lval_take(v, i); }
      }
      // Return early if 0 or 1 element
      if (v->count == 0) { return v; }
      if (v->count == 1) { return lval_take(v, 0); }
      // Check if first element is a function
      lval* f = lval_pop(v, 0);
      if (f->type != LVAL_FUN) {
	lval_del(f); lval_del(v);
	return lval_err("first element is not a function");
      }
      // Invoke function.
      // If builtin function, just call that
      if (f->builtin && f->builtin != builtin_if) { return f->builtin(e, v); }
      if (f->builtin && f->builtin == builtin_if) {
	LASSERT_NUM("if", v, 3);
	LASSERT_TYPE("if", v, 0, LVAL_NUM);
	LASSERT_TYPE("if", v, 1, LVAL_QEXPR);
	LASSERT_TYPE("if", v, 2, LVAL_QEXPR);
	lval* res;
	if (v->cell[0]->num) {
	  res = lval_take(v, 1);
	} else {
	  res = lval_take(v, 2);
	}
	res->type = LVAL_SEXPR;
	v = res;
	continue;
      }
      // add arguments to function lval
      f = add_arguments_to_function(f, v);
      // Add empty list as an argument if no variable arguments provided and the function could receive them
      f = add_empty_list_to_function_if_necessary(f);
      // Execute function if all arguments were supplied, else return the partially applied function
      if (f->formals->count != 0) {
	lval* result = lval_copy(f);
	lval_del(f);
        return result;
      } else {
	f->env->par = e;
	e = f->env;
	v = lval_copy(f->body);
	v->type = LVAL_SEXPR;
	continue;
      }
    default:
      return v;
    }
  }
}

int main(int argc, char** argv) {
  // Create some parsers
  Number = mpc_new("number");
  Symbol = mpc_new("symbol");
  String = mpc_new("string");
  Comment = mpc_new("comment");
  Sexpr = mpc_new("sexpr");
  Qexpr = mpc_new("qexpr");
  Expr = mpc_new("expr");
  Lispy = mpc_new("lispy");

  // Define the parsers
  mpca_lang(MPCA_LANG_DEFAULT,
    "\
    number  : /-?[0-9]+/ ;\
    symbol  : /[a-zA-Z0-9_+\\-*\\/\\\\=<>!&]+/ ;\
    string  : /\"(\\\\.|[^\"])*\"/ ;\
    comment : /;[^\\r\\n]*/ ;\
    sexpr   : '(' <expr>* ')' ;\
    qexpr   : '{' <expr>* '}' ;\
    expr    : <number> | <symbol> | <string> \
            | <comment> | <sexpr> | <qexpr> ;\
    lispy   : /^/ <expr>*  /$/ ;\
    ", Number, Symbol, String, Comment, Sexpr, Qexpr, Expr, Lispy);

  lenv* e = lenv_new();
  lenv_add_builtins(e);

  puts("Lispy version 0.000");
  puts("Press Ctrl+c to exit.\n");

  int dont_show_repl = 0;
  if (argc >= 2) {
    for (int i = 1; i < argc; i++) {
      if (strcmp(argv[i], "--no-repl") == 0) {
	dont_show_repl = 1;
	continue;
      }
      lval* args = lval_add(lval_sexpr(), lval_str(argv[i]));
      lval* result = builtin_load(e, args);
      if (result->type == LVAL_ERR) { lval_println(result); }
      lval_del(result);
    }
    if (dont_show_repl) {
      return 0;
    }
  }

  while (1) {
    char* input = readline("lispy> ");
    add_history(input);
    mpc_result_t r;
    if (mpc_parse("<stdin>", input, Lispy, &r)) {
      // mpc_ast_print(r.output);
      lval* result = lval_eval(e, lval_read(r.output));
      lval_println(result);
      lval_del(result);
      mpc_ast_delete(r.output);
    } else {
      mpc_err_print(r.error);
      mpc_err_delete(r.error);
    }
    free(input);
  }
  lenv_del(e);
  // Delete and cleanup parsers
  mpc_cleanup(8, Number, Symbol, String, Comment, Sexpr, Qexpr, Expr, Lispy);
  return 0;
}
